function result = exec_oleic_dummy_model(lambda)
    
    input_matrix = ones(17, 1);
    input_matrix(8) = 8.09e-08;
    input_matrix(11) = 1.52e-15;
    input_matrix(3:7) = lambda(1:5);
    input_matrix(9:10) = lambda(6:7);
    input_matrix(12:14) = lambda(8:10);
    
    t = [];
    Ymod = [];
    outputnum = 9;
    inputnum = 1;
    subsets = 1;
    J = 1;
    i = 1;
    tic;
    [ttmp, Ymodtmp, Z] = OLEICSIMPLE_ETH_sample_sce1_run(input_matrix, 1);
    t=[t,ttmp];
    Ymod=[Ymod,Ymodtmp]; 
    for j=1:outputnum(J)
        T(1:size(ttmp,1),sum(subsets(1:J-1).*outputnum(1:J-1))+j+outputnum(J)*(i-1))=ttmp(:,inputnum(J));
        YMOD(1:size(Ymodtmp,1),sum(subsets(1:J-1).*outputnum(1:J-1))+j+outputnum(J)*(i-1))=Ymodtmp(:,j);
    end
    RESULT=zeros(size(T,1),size(T,2)+size(Ymod,2));

    for j=1:outputnum
        RESULT(1:size(T,1),2*sum(subsets(1:J-1).*outputnum(1:J-1))+2*j-1+2*outputnum(J)*(i-1))=T(:,sum(subsets(1:J-1).*outputnum(1:J-1))+j+outputnum(J)*(i-1));
        RESULT(1:size(T,1),2*sum(subsets(1:J-1).*outputnum(1:J-1))+2*j+2*outputnum(J)*(i-1))=YMOD(:,sum(subsets(1:J-1).*outputnum(1:J-1))+j+outputnum(J)*(i-1));
    end


    omit=[2, 4, 6, 8, 10, 12, 14, 16, 18];
    result = RESULT(1,omit);
    
end