%% Function "DepletionEventsFcn" that detects when 99.5 % starting material is gone
% Thomas Berkemeier
% Last update: 2021/04/05

function [value,isterminal,direction] = DepletionEventsFcn(~,y,ODE,PARA)

%check termination condition
if ~isfield(ODE,'NY0')
    error('DepletionEventsFcn: termination condition NY0 not supplied');
end
if ~isfield(ODE,'timelimit')
    timelimit=inf;
else
    timelimit=ODE.timelimit;
end

%event: starting material gone
NY=y(4)*ODE.A(4);
for i=1:length(ODE.V)-4
    NY=NY+y(4+2*i)*ODE.V(4+i);
end

%generate outputs
value=[0.005*ODE.NY0-NY toc-timelimit];
isterminal=[1 1];
direction=[1 1];

return