%% KMMM Generated Model Code %%
% Developed by: Team Berkemeier
% Multiphase Chemistry Department
% Max Planck Institute for Chemistry, Mainz

% Model Compartments
% 1 | gas       |	Eqs. 1-1
% 2 | gas_near_surf|	Eqs. 2-2
% 3 | surface_sorp|	Eqs. 3-3
% 4 | surface_stat|	Eqs. 4-4
% 5 | fine_bulk |	Eqs. 5-6
% 6 | fine_bulk |	Eqs. 7-8
% 7 | fine_bulk |	Eqs. 9-10
% 8 | fine_bulk |	Eqs. 11-12
% 9 | fine_bulk |	Eqs. 13-14
% 10 | fine_bulk |	Eqs. 15-16
% 11 | fine_bulk |	Eqs. 17-18
% 12 | fine_bulk |	Eqs. 19-20
% 13 | fine_bulk |	Eqs. 21-22
% 14 | fine_bulk |	Eqs. 23-24
% 15 | fine_bulk |	Eqs. 25-26
% 16 | fine_bulk |	Eqs. 27-28
% 17 | fine_bulk |	Eqs. 29-30
% 18 | fine_bulk |	Eqs. 31-32
% 19 | fine_bulk |	Eqs. 33-34
% 20 | fine_bulk |	Eqs. 35-36
% 21 | fine_bulk |	Eqs. 37-38
% 22 | fine_bulk |	Eqs. 39-40
% 23 | fine_bulk |	Eqs. 41-42
% 24 | fine_bulk |	Eqs. 43-44
% 25 | bulk      |	Eqs. 45-46
% 26 | bulk      |	Eqs. 47-48
% 27 | bulk      |	Eqs. 49-50
% 28 | bulk      |	Eqs. 51-52
% 29 | bulk      |	Eqs. 53-54
% 30 | bulk      |	Eqs. 55-56
% 31 | bulk      |	Eqs. 57-58
% 32 | bulk      |	Eqs. 59-60
% 33 | bulk      |	Eqs. 61-62
% 34 | bulk      |	Eqs. 63-64
% 35 | bulk      |	Eqs. 65-66
% 36 | bulk      |	Eqs. 67-68
% 37 | bulk      |	Eqs. 69-70
% 38 | bulk      |	Eqs. 71-72
% 39 | bulk      |	Eqs. 73-74
% 40 | bulk      |	Eqs. 75-76
% 41 | bulk      |	Eqs. 77-78
% 42 | bulk      |	Eqs. 79-80
% 43 | bulk      |	Eqs. 81-82
% 44 | bulk      |	Eqs. 83-84
% 45 | bulk      |	Eqs. 85-86
% 46 | bulk      |	Eqs. 87-88
% 47 | bulk      |	Eqs. 89-90
% 48 | bulk      |	Eqs. 91-92
% 49 | bulk      |	Eqs. 93-94
% 50 | bulk      |	Eqs. 95-96
% 51 | bulk      |	Eqs. 97-98
% 52 | bulk      |	Eqs. 99-100
% 53 | bulk      |	Eqs. 101-102
% 54 | bulk      |	Eqs. 103-104
% 55 | bulk      |	Eqs. 105-106
% 56 | bulk      |	Eqs. 107-108
% 57 | bulk      |	Eqs. 109-110
% 58 | bulk      |	Eqs. 111-112
% 59 | bulk      |	Eqs. 113-114
% 60 | bulk      |	Eqs. 115-116
% 61 | bulk      |	Eqs. 117-118
% 62 | bulk      |	Eqs. 119-120
% 63 | bulk      |	Eqs. 121-122
% 64 | bulk      |	Eqs. 123-124
% 65 | bulk      |	Eqs. 125-126
% 66 | bulk      |	Eqs. 127-128
% 67 | bulk      |	Eqs. 129-130
% 68 | bulk      |	Eqs. 131-132
% 69 | bulk      |	Eqs. 133-134
% 70 | bulk      |	Eqs. 135-136
% 71 | bulk      |	Eqs. 137-138
% 72 | bulk      |	Eqs. 139-140
% 73 | bulk      |	Eqs. 141-142
% 74 | bulk      |	Eqs. 143-144
% 75 | bulk      |	Eqs. 145-146
% 76 | bulk      |	Eqs. 147-148
% 77 | bulk      |	Eqs. 149-150
% 78 | bulk      |	Eqs. 151-152
% 79 | bulk      |	Eqs. 153-154
% 80 | bulk      |	Eqs. 155-156
% 81 | bulk      |	Eqs. 157-158
% 82 | bulk      |	Eqs. 159-160
% 83 | bulk      |	Eqs. 161-162
% 84 | bulk      |	Eqs. 163-164
% 85 | bulk      |	Eqs. 165-166
% 86 | bulk      |	Eqs. 167-168
% 87 | bulk      |	Eqs. 169-170
% 88 | bulk      |	Eqs. 171-172
% 89 | bulk      |	Eqs. 173-174
% 90 | bulk      |	Eqs. 175-176
% 91 | bulk      |	Eqs. 177-178
% 92 | bulk      |	Eqs. 179-180
% 93 | bulk      |	Eqs. 181-182
% 94 | bulk      |	Eqs. 183-184
% 95 | bulk      |	Eqs. 185-186
% 96 | bulk      |	Eqs. 187-188
% 97 | bulk      |	Eqs. 189-190
% 98 | bulk      |	Eqs. 191-192
% 99 | bulk      |	Eqs. 193-194
% 100 | bulk      |	Eqs. 195-196
% 101 | bulk      |	Eqs. 197-198
% 102 | bulk      |	Eqs. 199-200
% 103 | bulk      |	Eqs. 201-202
% 104 | bulk      |	Eqs. 203-204

% Model Species |	DIFEQ Reference
% O3            |	1, 2, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 31, 33, 35, 37, 39, 41, 43, 45, 47, 49, 51, 53, 55, 57, 59, 61, 63, 65, 67, 69, 71, 73, 75, 77, 79, 81, 83, 85, 87, 89, 91, 93, 95, 97, 99, 101, 103, 105, 107, 109, 111, 113, 115, 117, 119, 121, 123, 125, 127, 129, 131, 133, 135, 137, 139, 141, 143, 145, 147, 149, 151, 153, 155, 157, 159, 161, 163, 165, 167, 169, 171, 173, 175, 177, 179, 181, 183, 185, 187, 189, 191, 193, 195, 197, 199, 201, 203
% OL            |	4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32, 34, 36, 38, 40, 42, 44, 46, 48, 50, 52, 54, 56, 58, 60, 62, 64, 66, 68, 70, 72, 74, 76, 78, 80, 82, 84, 86, 88, 90, 92, 94, 96, 98, 100, 102, 104, 106, 108, 110, 112, 114, 116, 118, 120, 122, 124, 126, 128, 130, 132, 134, 136, 138, 140, 142, 144, 146, 148, 150, 152, 154, 156, 158, 160, 162, 164, 166, 168, 170, 172, 174, 176, 178, 180, 182, 184, 186, 188, 190, 192, 194, 196, 198, 200, 202, 204

% Chemical Mechanism
% R(1)	O3+OL=	surface_sorp/surface_stat
% R(2)	O3+OL=	fine_bulk
% R(3)	O3+OL=	bulk

%% CODE %%

function [X,Y,Z,species,RES]=OLEICSIMPLE_ETH_sample_sce1_run(inpt,subset)

% Created: 31-Jul-2023 11:19:39

%%% SPECIES AND LAYERS %%%

species={'O3'; 'OL'; };

compartments={'gas'; 'gas_near_surf'; 'surface_sorp'; 'surface_stat'; 'fine_bulk';
'fine_bulk'; 'fine_bulk'; 'fine_bulk'; 'fine_bulk'; 'fine_bulk';
'fine_bulk'; 'fine_bulk'; 'fine_bulk'; 'fine_bulk'; 'fine_bulk';
'fine_bulk'; 'fine_bulk'; 'fine_bulk'; 'fine_bulk'; 'fine_bulk';
'fine_bulk'; 'fine_bulk'; 'fine_bulk'; 'fine_bulk'; 'bulk';
'bulk'; 'bulk'; 'bulk'; 'bulk'; 'bulk';
'bulk'; 'bulk'; 'bulk'; 'bulk'; 'bulk';
'bulk'; 'bulk'; 'bulk'; 'bulk'; 'bulk';
'bulk'; 'bulk'; 'bulk'; 'bulk'; 'bulk';
'bulk'; 'bulk'; 'bulk'; 'bulk'; 'bulk';
'bulk'; 'bulk'; 'bulk'; 'bulk'; 'bulk';
'bulk'; 'bulk'; 'bulk'; 'bulk'; 'bulk';
'bulk'; 'bulk'; 'bulk'; 'bulk'; 'bulk';
'bulk'; 'bulk'; 'bulk'; 'bulk'; 'bulk';
'bulk'; 'bulk'; 'bulk'; 'bulk'; 'bulk';
'bulk'; 'bulk'; 'bulk'; 'bulk'; 'bulk';
'bulk'; 'bulk'; 'bulk'; 'bulk'; 'bulk';
'bulk'; 'bulk'; 'bulk'; 'bulk'; 'bulk';
'bulk'; 'bulk'; 'bulk'; 'bulk'; 'bulk';
'bulk'; 'bulk'; 'bulk'; 'bulk'; 'bulk';
'bulk'; 'bulk'; 'bulk'; 'bulk'; };

ref_list=[1, 2, 3, 0, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 31, 33, 35, 37, 39, 41, 43, 45, 47, 49, 51, 53, 55, 57, 59, 61, 63, 65, 67, 69, 71, 73, 75, 77, 79, 81, 83, 85, 87, 89, 91, 93, 95, 97, 99, 101, 103, 105, 107, 109, 111, 113, 115, 117, 119, 121, 123, 125, 127, 129, 131, 133, 135, 137, 139, 141, 143, 145, 147, 149, 151, 153, 155, 157, 159, 161, 163, 165, 167, 169, 171, 173, 175, 177, 179, 181, 183, 185, 187, 189, 191, 193, 195, 197, 199, 201, 203;
0, 0, 0, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32, 34, 36, 38, 40, 42, 44, 46, 48, 50, 52, 54, 56, 58, 60, 62, 64, 66, 68, 70, 72, 74, 76, 78, 80, 82, 84, 86, 88, 90, 92, 94, 96, 98, 100, 102, 104, 106, 108, 110, 112, 114, 116, 118, 120, 122, 124, 126, 128, 130, 132, 134, 136, 138, 140, 142, 144, 146, 148, 150, 152, 154, 156, 158, 160, 162, 164, 166, 168, 170, 172, 174, 176, 178, 180, 182, 184, 186, 188, 190, 192, 194, 196, 198, 200, 202, 204;
];

%%% OPT %%%

opt.n=10000;opt.T=298;opt.AbsTol=0.1;opt.RelTol=0.001;opt.start=-3;
opt.stop=9;opt.fine_bulkL=20;opt.bulkL=80;opt.tspace={'log'};

%%% DATABASE %%%

%% O3 %%
PARA.O3.M=48;
PARA.O3.Dg=0.14;
PARA.O3.delta=3.89e-08;
PARA.O3.a0=0.1;
PARA.O3.Db=1e-05;
PARA.O3.Hcp=0.000484;
PARA.O3.Kfine_bulkbulk=1;

%% OL %%
PARA.OL.M=282.46;
PARA.OL.delta=8.09e-08;
PARA.OL.rsites=1;
PARA.OL.a0=1;
PARA.OL.Db=1.9e-07;
PARA.OL.Kfine_bulkbulk=1;
PARA.OL.sigma=6.5e-15;


%%% INPUT %%%

PARA.O3.kbsxfac=inpt(1);PARA.OL.kssbyfac=inpt(2);ODE.SurfRate=inpt(3);ODE.BulkRate=inpt(4);PARA.O3.Db=inpt(5);
PARA.OL.Db=inpt(6);PARA.O3.Hcp=inpt(7);PARA.OL.Ksurface_statfine_bulk=inpt(8);PARA.O3.Td=inpt(9);PARA.O3.a0=inpt(10);
PARA.O3.sigma=inpt(11);rout=inpt(12);O3conc=inpt(13);OLconc=inpt(14);p(1)=inpt(15);
p(2)=inpt(16);p(3)=inpt(17);

%%% EXP DATA %%%

DATA=[0.0001,1,0.0001,1,0.0001,1,0.0001,1,0.0001,1,0.0001,1,0.0001,1,0.0001,1,0.0001,1;
];data.input={'t','t','t','t','t','t','t','t','t'};data.output={'lifetime_10','lifetime_20','lifetime_30','lifetime_40','lifetime_50','lifetime_60','lifetime_70','lifetime_80','lifetime_90'};data.LSF={'absolute','absolute','absolute','absolute','absolute','absolute','absolute','absolute','absolute'};data.length=[1,1,1,1,1,1,1,1,1];data.plotnum=[1,1,1,1,1,1,1,1,1];data.x_label={'progress','progress','progress','progress','progress','progress','progress','progress','progress'};data.y_label={'timetodeplete','timetodeplete','timetodeplete','timetodeplete','timetodeplete','timetodeplete','timetodeplete','timetodeplete','timetodeplete'};

%%% INITIAL %%%

y0_raw=zeros([204    1]);
y0_raw(1,1:1)=[O3conc, ];
y0_raw(2,1:1)=[O3conc, ];
y0_raw(4,1:1)=[1/(1/(PARA.OL.Ksurface_statfine_bulk*OLconc)+PARA.OL.delta^2), ];
y0_raw(6,1:1)=[OLconc, ];
y0_raw(8,1:1)=[OLconc, ];
y0_raw(10,1:1)=[OLconc, ];
y0_raw(12,1:1)=[OLconc, ];
y0_raw(14,1:1)=[OLconc, ];
y0_raw(16,1:1)=[OLconc, ];
y0_raw(18,1:1)=[OLconc, ];
y0_raw(20,1:1)=[OLconc, ];
y0_raw(22,1:1)=[OLconc, ];
y0_raw(24,1:1)=[OLconc, ];
y0_raw(26,1:1)=[OLconc, ];
y0_raw(28,1:1)=[OLconc, ];
y0_raw(30,1:1)=[OLconc, ];
y0_raw(32,1:1)=[OLconc, ];
y0_raw(34,1:1)=[OLconc, ];
y0_raw(36,1:1)=[OLconc, ];
y0_raw(38,1:1)=[OLconc, ];
y0_raw(40,1:1)=[OLconc, ];
y0_raw(42,1:1)=[OLconc, ];
y0_raw(44,1:1)=[OLconc, ];
y0_raw(46,1:1)=[OLconc, ];
y0_raw(48,1:1)=[OLconc, ];
y0_raw(50,1:1)=[OLconc, ];
y0_raw(52,1:1)=[OLconc, ];
y0_raw(54,1:1)=[OLconc, ];
y0_raw(56,1:1)=[OLconc, ];
y0_raw(58,1:1)=[OLconc, ];
y0_raw(60,1:1)=[OLconc, ];
y0_raw(62,1:1)=[OLconc, ];
y0_raw(64,1:1)=[OLconc, ];
y0_raw(66,1:1)=[OLconc, ];
y0_raw(68,1:1)=[OLconc, ];
y0_raw(70,1:1)=[OLconc, ];
y0_raw(72,1:1)=[OLconc, ];
y0_raw(74,1:1)=[OLconc, ];
y0_raw(76,1:1)=[OLconc, ];
y0_raw(78,1:1)=[OLconc, ];
y0_raw(80,1:1)=[OLconc, ];
y0_raw(82,1:1)=[OLconc, ];
y0_raw(84,1:1)=[OLconc, ];
y0_raw(86,1:1)=[OLconc, ];
y0_raw(88,1:1)=[OLconc, ];
y0_raw(90,1:1)=[OLconc, ];
y0_raw(92,1:1)=[OLconc, ];
y0_raw(94,1:1)=[OLconc, ];
y0_raw(96,1:1)=[OLconc, ];
y0_raw(98,1:1)=[OLconc, ];
y0_raw(100,1:1)=[OLconc, ];
y0_raw(102,1:1)=[OLconc, ];
y0_raw(104,1:1)=[OLconc, ];
y0_raw(106,1:1)=[OLconc, ];
y0_raw(108,1:1)=[OLconc, ];
y0_raw(110,1:1)=[OLconc, ];
y0_raw(112,1:1)=[OLconc, ];
y0_raw(114,1:1)=[OLconc, ];
y0_raw(116,1:1)=[OLconc, ];
y0_raw(118,1:1)=[OLconc, ];
y0_raw(120,1:1)=[OLconc, ];
y0_raw(122,1:1)=[OLconc, ];
y0_raw(124,1:1)=[OLconc, ];
y0_raw(126,1:1)=[OLconc, ];
y0_raw(128,1:1)=[OLconc, ];
y0_raw(130,1:1)=[OLconc, ];
y0_raw(132,1:1)=[OLconc, ];
y0_raw(134,1:1)=[OLconc, ];
y0_raw(136,1:1)=[OLconc, ];
y0_raw(138,1:1)=[OLconc, ];
y0_raw(140,1:1)=[OLconc, ];
y0_raw(142,1:1)=[OLconc, ];
y0_raw(144,1:1)=[OLconc, ];
y0_raw(146,1:1)=[OLconc, ];
y0_raw(148,1:1)=[OLconc, ];
y0_raw(150,1:1)=[OLconc, ];
y0_raw(152,1:1)=[OLconc, ];
y0_raw(154,1:1)=[OLconc, ];
y0_raw(156,1:1)=[OLconc, ];
y0_raw(158,1:1)=[OLconc, ];
y0_raw(160,1:1)=[OLconc, ];
y0_raw(162,1:1)=[OLconc, ];
y0_raw(164,1:1)=[OLconc, ];
y0_raw(166,1:1)=[OLconc, ];
y0_raw(168,1:1)=[OLconc, ];
y0_raw(170,1:1)=[OLconc, ];
y0_raw(172,1:1)=[OLconc, ];
y0_raw(174,1:1)=[OLconc, ];
y0_raw(176,1:1)=[OLconc, ];
y0_raw(178,1:1)=[OLconc, ];
y0_raw(180,1:1)=[OLconc, ];
y0_raw(182,1:1)=[OLconc, ];
y0_raw(184,1:1)=[OLconc, ];
y0_raw(186,1:1)=[OLconc, ];
y0_raw(188,1:1)=[OLconc, ];
y0_raw(190,1:1)=[OLconc, ];
y0_raw(192,1:1)=[OLconc, ];
y0_raw(194,1:1)=[OLconc, ];
y0_raw(196,1:1)=[OLconc, ];
y0_raw(198,1:1)=[OLconc, ];
y0_raw(200,1:1)=[OLconc, ];
y0_raw(202,1:1)=[OLconc, ];
y0_raw(204,1:1)=[OLconc, ];
y0=y0_raw(:,subset);

%%% PRE PROCESSING %%%

R=8.314e7;  R2=82.0578;  

PARA.O3.KHcc=PARA.O3.Hcp*R2*opt.T; %Henry's law coefficient of X in Y []
PARA.O3.MTV=sqrt(8*R*opt.T/pi/PARA.O3.M);  %thermal velocity [cm s-1]
PARA.O3.MFP=1.7*PARA.O3.Dg/PARA.O3.MTV;  %mean free path in air [cm]
PARA.OL.MTV=sqrt(8*R*opt.T/pi/PARA.OL.M);  %thermal velocity [cm s-1]
ODE.ref_list=ref_list;

%GAS PHASE DIFFUSION%

R=8.314e7;                                
%gas constant [g cm2 s-2 K-1 mol-1]
PARA.O3.MTV=sqrt(8*R*opt.T/pi/PARA.O3.M); 
%thermal velocity [cm s-1]
mfp=1.7*PARA.O3.Dg/PARA.O3.MTV;           
%mean free path in air [cm]

PARA.O3.KHcc=PARA.O3.Hcp*82.0578*opt.T;  
%Henry's law coefficient of X in Y []

%%%%%%%%%%%%%%%%%%GEOMETRY%%%%%%%%%%%%%%%%%%%%%%%

%LAYER SPACINGS
L.fine=opt.fine_bulkL;                   %number of fine bulk layers []
L.minsize=PARA.O3.delta;                 %mininum layer spacing [cm]
lrd=sqrt(PARA.O3.Db/(ODE.BulkRate*y0_raw(6,1:1)));%reacto-diffusive length [cm]
delta(2)=PARA.OL.delta;                %effective diameter of reactive site [cm] 
thcknss=min(max(5*lrd,L.fine*L.minsize),rout-delta(2));       %thickness of treated bulk film [cm] (this is delta then?)
L.finesize=thcknss./L.fine;               %thickness of fine bulk layer [cm] (delta fine size) 
PARA.OL.sigma=delta(2)^2;
if (opt.fine_bulkL+opt.bulkL)*L.finesize > rout-delta(2)
	L.finesize=(rout-delta(2))/(opt.fine_bulkL+opt.bulkL);
	L.size=L.finesize;
else
	Dcoarse=rout-delta(2)-opt.fine_bulkL*L.finesize;
	L.size=Dcoarse/opt.bulkL;
end

%L.tot is the total number of layers in the model, including gas, surface and bulk
L.tot=4+opt.fine_bulkL+opt.bulkL;

%SURFACE AREA AND VOLUME
ODE.A=zeros(L.tot-1,1);ODE.V=zeros(L.tot,1);r=zeros(opt.fine_bulkL+opt.bulkL,1);

%r is the position of the "top" of all bulk layers in the model, ranging from 1 to opt.fine_bulkL+opt.bulkL
for i=1:opt.fine_bulkL
	r(i)=rout-delta(2)-(i-1)*L.finesize;
end
for i=opt.fine_bulkL+1:opt.fine_bulkL+opt.bulkL

	r(i)=rout-delta(2)-opt.fine_bulkL*L.finesize-(i-opt.fine_bulkL-1)*L.size;
end

for i=2:L.tot-4
	%surface area of bulk layers 1:n-1 [cm2] 
	ODE.A(i+2)=4*pi*r(i)^2;
end
for i=1:L.tot-5
	%volume of bulk layers 1:n-1 [cm3]
	ODE.V(i+4)=4/3*pi*(r(i)^3-r(1+i)^3);
end

%volume of bulk layer n [cm3]
ODE.V(L.tot)=4/3*pi*((r(L.tot-4))^3);
%surface area of bulk layer n [cm2]
ODE.A(L.tot-1)=4*pi*(r(L.tot-4))^2; 
%f.s. gas diffusion shell 
ODE.V(1)=4/3*pi*((2*mfp+rout+PARA.O3.delta)^3-(mfp+rout+PARA.O3.delta)^3);
%n.s. gas diffusion shell
ODE.A(1)=4*pi*(rout+mfp+PARA.O3.delta)^2;
ODE.V(2)=4/3*pi*((mfp+rout+PARA.O3.delta)^3-(rout+PARA.O3.delta)^3);

%sorption layer
ODE.A(2)=4*pi*(rout+PARA.O3.delta)^2;
ODE.V(3)=4/3*pi*((rout+PARA.O3.delta)^3-rout^3);

%static surface layer
ODE.A(3)=4*pi*rout^2;
ODE.V(4)=4/3*pi*((rout)^3-(rout-delta(2))^3);

%Convert to ODE structure%%%%%%%%%%%%%

ODE.D(1)=mfp;                                  %thickness gas layer 1 [cm]
ODE.D(2)=mfp;                                  %thickness gas layer 1 [cm]
ODE.D(3)=PARA.O3.delta;                        %thickness sorption layer [cm]
ODE.D(4)=delta(2);                             %thickness static surface layer [cm] 
ODE.D(5:opt.fine_bulkL+3)=L.finesize;          %thickness fine bulk layer [cm]
ODE.D(opt.fine_bulkL+4:opt.fine_bulkL+opt.bulkL+4)=L.size;    %tickness coarse bulk layer [cm] 
ODE.O3gas=y0_raw(1,1);

%initial number of molecules 
ODE.NY0=y0_raw(4)*ODE.A(4);
for i=1:(opt.fine_bulkL+opt.bulkL)
	ODE.NY0=ODE.NY0+y0_raw(4+2*i)*ODE.V(4+i);
end 

%%% TRANSPORT %%%

ODE.kg1(1)=PARA.O3.Dg/min(ODE.D(1),ODE.D(2));
ODE.kd1(1)=1/PARA.O3.Td;
ODE.kbs1(1)=2*PARA.O3.Db/(ODE.D(3)+2*ODE.D(4)+ODE.D(5));
ODE.kbs2(2)=2*PARA.OL.Db/(ODE.D(4)+ODE.D(5));
ODE.ksb2(2)=2*PARA.OL.Db/PARA.OL.Ksurface_statfine_bulk/(ODE.D(4)+ODE.D(5));
ODE.kb(1,1)=2*PARA.O3.Db/(ODE.D(5)+ODE.D(6));
ODE.kb(2,1)=2*PARA.OL.Db/(ODE.D(5)+ODE.D(6));
ODE.kb(1,2)=2*PARA.O3.Db/(ODE.D(6)+ODE.D(7));
ODE.kb(2,2)=2*PARA.OL.Db/(ODE.D(6)+ODE.D(7));
ODE.kb(1,3)=2*PARA.O3.Db/(ODE.D(7)+ODE.D(8));
ODE.kb(2,3)=2*PARA.OL.Db/(ODE.D(7)+ODE.D(8));
ODE.kb(1,4)=2*PARA.O3.Db/(ODE.D(8)+ODE.D(9));
ODE.kb(2,4)=2*PARA.OL.Db/(ODE.D(8)+ODE.D(9));
ODE.kb(1,5)=2*PARA.O3.Db/(ODE.D(9)+ODE.D(10));
ODE.kb(2,5)=2*PARA.OL.Db/(ODE.D(9)+ODE.D(10));
ODE.kb(1,6)=2*PARA.O3.Db/(ODE.D(10)+ODE.D(11));
ODE.kb(2,6)=2*PARA.OL.Db/(ODE.D(10)+ODE.D(11));
ODE.kb(1,7)=2*PARA.O3.Db/(ODE.D(11)+ODE.D(12));
ODE.kb(2,7)=2*PARA.OL.Db/(ODE.D(11)+ODE.D(12));
ODE.kb(1,8)=2*PARA.O3.Db/(ODE.D(12)+ODE.D(13));
ODE.kb(2,8)=2*PARA.OL.Db/(ODE.D(12)+ODE.D(13));
ODE.kb(1,9)=2*PARA.O3.Db/(ODE.D(13)+ODE.D(14));
ODE.kb(2,9)=2*PARA.OL.Db/(ODE.D(13)+ODE.D(14));
ODE.kb(1,10)=2*PARA.O3.Db/(ODE.D(14)+ODE.D(15));
ODE.kb(2,10)=2*PARA.OL.Db/(ODE.D(14)+ODE.D(15));
ODE.kb(1,11)=2*PARA.O3.Db/(ODE.D(15)+ODE.D(16));
ODE.kb(2,11)=2*PARA.OL.Db/(ODE.D(15)+ODE.D(16));
ODE.kb(1,12)=2*PARA.O3.Db/(ODE.D(16)+ODE.D(17));
ODE.kb(2,12)=2*PARA.OL.Db/(ODE.D(16)+ODE.D(17));
ODE.kb(1,13)=2*PARA.O3.Db/(ODE.D(17)+ODE.D(18));
ODE.kb(2,13)=2*PARA.OL.Db/(ODE.D(17)+ODE.D(18));
ODE.kb(1,14)=2*PARA.O3.Db/(ODE.D(18)+ODE.D(19));
ODE.kb(2,14)=2*PARA.OL.Db/(ODE.D(18)+ODE.D(19));
ODE.kb(1,15)=2*PARA.O3.Db/(ODE.D(19)+ODE.D(20));
ODE.kb(2,15)=2*PARA.OL.Db/(ODE.D(19)+ODE.D(20));
ODE.kb(1,16)=2*PARA.O3.Db/(ODE.D(20)+ODE.D(21));
ODE.kb(2,16)=2*PARA.OL.Db/(ODE.D(20)+ODE.D(21));
ODE.kb(1,17)=2*PARA.O3.Db/(ODE.D(21)+ODE.D(22));
ODE.kb(2,17)=2*PARA.OL.Db/(ODE.D(21)+ODE.D(22));
ODE.kb(1,18)=2*PARA.O3.Db/(ODE.D(22)+ODE.D(23));
ODE.kb(2,18)=2*PARA.OL.Db/(ODE.D(22)+ODE.D(23));
ODE.kb(1,19)=2*PARA.O3.Db/(ODE.D(23)+ODE.D(24));
ODE.kb(2,19)=2*PARA.OL.Db/(ODE.D(23)+ODE.D(24));
ODE.kb(1,20)=2*PARA.O3.Db/(ODE.D(24)+ODE.D(25));
ODE.kb(2,20)=2*PARA.OL.Db/(ODE.D(24)+ODE.D(25));
ODE.kb(1,21)=2*PARA.O3.Db/(ODE.D(25)+ODE.D(26));
ODE.kb(2,21)=2*PARA.OL.Db/(ODE.D(25)+ODE.D(26));
ODE.kb(1,22)=2*PARA.O3.Db/(ODE.D(26)+ODE.D(27));
ODE.kb(2,22)=2*PARA.OL.Db/(ODE.D(26)+ODE.D(27));
ODE.kb(1,23)=2*PARA.O3.Db/(ODE.D(27)+ODE.D(28));
ODE.kb(2,23)=2*PARA.OL.Db/(ODE.D(27)+ODE.D(28));
ODE.kb(1,24)=2*PARA.O3.Db/(ODE.D(28)+ODE.D(29));
ODE.kb(2,24)=2*PARA.OL.Db/(ODE.D(28)+ODE.D(29));
ODE.kb(1,25)=2*PARA.O3.Db/(ODE.D(29)+ODE.D(30));
ODE.kb(2,25)=2*PARA.OL.Db/(ODE.D(29)+ODE.D(30));
ODE.kb(1,26)=2*PARA.O3.Db/(ODE.D(30)+ODE.D(31));
ODE.kb(2,26)=2*PARA.OL.Db/(ODE.D(30)+ODE.D(31));
ODE.kb(1,27)=2*PARA.O3.Db/(ODE.D(31)+ODE.D(32));
ODE.kb(2,27)=2*PARA.OL.Db/(ODE.D(31)+ODE.D(32));
ODE.kb(1,28)=2*PARA.O3.Db/(ODE.D(32)+ODE.D(33));
ODE.kb(2,28)=2*PARA.OL.Db/(ODE.D(32)+ODE.D(33));
ODE.kb(1,29)=2*PARA.O3.Db/(ODE.D(33)+ODE.D(34));
ODE.kb(2,29)=2*PARA.OL.Db/(ODE.D(33)+ODE.D(34));
ODE.kb(1,30)=2*PARA.O3.Db/(ODE.D(34)+ODE.D(35));
ODE.kb(2,30)=2*PARA.OL.Db/(ODE.D(34)+ODE.D(35));
ODE.kb(1,31)=2*PARA.O3.Db/(ODE.D(35)+ODE.D(36));
ODE.kb(2,31)=2*PARA.OL.Db/(ODE.D(35)+ODE.D(36));
ODE.kb(1,32)=2*PARA.O3.Db/(ODE.D(36)+ODE.D(37));
ODE.kb(2,32)=2*PARA.OL.Db/(ODE.D(36)+ODE.D(37));
ODE.kb(1,33)=2*PARA.O3.Db/(ODE.D(37)+ODE.D(38));
ODE.kb(2,33)=2*PARA.OL.Db/(ODE.D(37)+ODE.D(38));
ODE.kb(1,34)=2*PARA.O3.Db/(ODE.D(38)+ODE.D(39));
ODE.kb(2,34)=2*PARA.OL.Db/(ODE.D(38)+ODE.D(39));
ODE.kb(1,35)=2*PARA.O3.Db/(ODE.D(39)+ODE.D(40));
ODE.kb(2,35)=2*PARA.OL.Db/(ODE.D(39)+ODE.D(40));
ODE.kb(1,36)=2*PARA.O3.Db/(ODE.D(40)+ODE.D(41));
ODE.kb(2,36)=2*PARA.OL.Db/(ODE.D(40)+ODE.D(41));
ODE.kb(1,37)=2*PARA.O3.Db/(ODE.D(41)+ODE.D(42));
ODE.kb(2,37)=2*PARA.OL.Db/(ODE.D(41)+ODE.D(42));
ODE.kb(1,38)=2*PARA.O3.Db/(ODE.D(42)+ODE.D(43));
ODE.kb(2,38)=2*PARA.OL.Db/(ODE.D(42)+ODE.D(43));
ODE.kb(1,39)=2*PARA.O3.Db/(ODE.D(43)+ODE.D(44));
ODE.kb(2,39)=2*PARA.OL.Db/(ODE.D(43)+ODE.D(44));
ODE.kb(1,40)=2*PARA.O3.Db/(ODE.D(44)+ODE.D(45));
ODE.kb(2,40)=2*PARA.OL.Db/(ODE.D(44)+ODE.D(45));
ODE.kb(1,41)=2*PARA.O3.Db/(ODE.D(45)+ODE.D(46));
ODE.kb(2,41)=2*PARA.OL.Db/(ODE.D(45)+ODE.D(46));
ODE.kb(1,42)=2*PARA.O3.Db/(ODE.D(46)+ODE.D(47));
ODE.kb(2,42)=2*PARA.OL.Db/(ODE.D(46)+ODE.D(47));
ODE.kb(1,43)=2*PARA.O3.Db/(ODE.D(47)+ODE.D(48));
ODE.kb(2,43)=2*PARA.OL.Db/(ODE.D(47)+ODE.D(48));
ODE.kb(1,44)=2*PARA.O3.Db/(ODE.D(48)+ODE.D(49));
ODE.kb(2,44)=2*PARA.OL.Db/(ODE.D(48)+ODE.D(49));
ODE.kb(1,45)=2*PARA.O3.Db/(ODE.D(49)+ODE.D(50));
ODE.kb(2,45)=2*PARA.OL.Db/(ODE.D(49)+ODE.D(50));
ODE.kb(1,46)=2*PARA.O3.Db/(ODE.D(50)+ODE.D(51));
ODE.kb(2,46)=2*PARA.OL.Db/(ODE.D(50)+ODE.D(51));
ODE.kb(1,47)=2*PARA.O3.Db/(ODE.D(51)+ODE.D(52));
ODE.kb(2,47)=2*PARA.OL.Db/(ODE.D(51)+ODE.D(52));
ODE.kb(1,48)=2*PARA.O3.Db/(ODE.D(52)+ODE.D(53));
ODE.kb(2,48)=2*PARA.OL.Db/(ODE.D(52)+ODE.D(53));
ODE.kb(1,49)=2*PARA.O3.Db/(ODE.D(53)+ODE.D(54));
ODE.kb(2,49)=2*PARA.OL.Db/(ODE.D(53)+ODE.D(54));
ODE.kb(1,50)=2*PARA.O3.Db/(ODE.D(54)+ODE.D(55));
ODE.kb(2,50)=2*PARA.OL.Db/(ODE.D(54)+ODE.D(55));
ODE.kb(1,51)=2*PARA.O3.Db/(ODE.D(55)+ODE.D(56));
ODE.kb(2,51)=2*PARA.OL.Db/(ODE.D(55)+ODE.D(56));
ODE.kb(1,52)=2*PARA.O3.Db/(ODE.D(56)+ODE.D(57));
ODE.kb(2,52)=2*PARA.OL.Db/(ODE.D(56)+ODE.D(57));
ODE.kb(1,53)=2*PARA.O3.Db/(ODE.D(57)+ODE.D(58));
ODE.kb(2,53)=2*PARA.OL.Db/(ODE.D(57)+ODE.D(58));
ODE.kb(1,54)=2*PARA.O3.Db/(ODE.D(58)+ODE.D(59));
ODE.kb(2,54)=2*PARA.OL.Db/(ODE.D(58)+ODE.D(59));
ODE.kb(1,55)=2*PARA.O3.Db/(ODE.D(59)+ODE.D(60));
ODE.kb(2,55)=2*PARA.OL.Db/(ODE.D(59)+ODE.D(60));
ODE.kb(1,56)=2*PARA.O3.Db/(ODE.D(60)+ODE.D(61));
ODE.kb(2,56)=2*PARA.OL.Db/(ODE.D(60)+ODE.D(61));
ODE.kb(1,57)=2*PARA.O3.Db/(ODE.D(61)+ODE.D(62));
ODE.kb(2,57)=2*PARA.OL.Db/(ODE.D(61)+ODE.D(62));
ODE.kb(1,58)=2*PARA.O3.Db/(ODE.D(62)+ODE.D(63));
ODE.kb(2,58)=2*PARA.OL.Db/(ODE.D(62)+ODE.D(63));
ODE.kb(1,59)=2*PARA.O3.Db/(ODE.D(63)+ODE.D(64));
ODE.kb(2,59)=2*PARA.OL.Db/(ODE.D(63)+ODE.D(64));
ODE.kb(1,60)=2*PARA.O3.Db/(ODE.D(64)+ODE.D(65));
ODE.kb(2,60)=2*PARA.OL.Db/(ODE.D(64)+ODE.D(65));
ODE.kb(1,61)=2*PARA.O3.Db/(ODE.D(65)+ODE.D(66));
ODE.kb(2,61)=2*PARA.OL.Db/(ODE.D(65)+ODE.D(66));
ODE.kb(1,62)=2*PARA.O3.Db/(ODE.D(66)+ODE.D(67));
ODE.kb(2,62)=2*PARA.OL.Db/(ODE.D(66)+ODE.D(67));
ODE.kb(1,63)=2*PARA.O3.Db/(ODE.D(67)+ODE.D(68));
ODE.kb(2,63)=2*PARA.OL.Db/(ODE.D(67)+ODE.D(68));
ODE.kb(1,64)=2*PARA.O3.Db/(ODE.D(68)+ODE.D(69));
ODE.kb(2,64)=2*PARA.OL.Db/(ODE.D(68)+ODE.D(69));
ODE.kb(1,65)=2*PARA.O3.Db/(ODE.D(69)+ODE.D(70));
ODE.kb(2,65)=2*PARA.OL.Db/(ODE.D(69)+ODE.D(70));
ODE.kb(1,66)=2*PARA.O3.Db/(ODE.D(70)+ODE.D(71));
ODE.kb(2,66)=2*PARA.OL.Db/(ODE.D(70)+ODE.D(71));
ODE.kb(1,67)=2*PARA.O3.Db/(ODE.D(71)+ODE.D(72));
ODE.kb(2,67)=2*PARA.OL.Db/(ODE.D(71)+ODE.D(72));
ODE.kb(1,68)=2*PARA.O3.Db/(ODE.D(72)+ODE.D(73));
ODE.kb(2,68)=2*PARA.OL.Db/(ODE.D(72)+ODE.D(73));
ODE.kb(1,69)=2*PARA.O3.Db/(ODE.D(73)+ODE.D(74));
ODE.kb(2,69)=2*PARA.OL.Db/(ODE.D(73)+ODE.D(74));
ODE.kb(1,70)=2*PARA.O3.Db/(ODE.D(74)+ODE.D(75));
ODE.kb(2,70)=2*PARA.OL.Db/(ODE.D(74)+ODE.D(75));
ODE.kb(1,71)=2*PARA.O3.Db/(ODE.D(75)+ODE.D(76));
ODE.kb(2,71)=2*PARA.OL.Db/(ODE.D(75)+ODE.D(76));
ODE.kb(1,72)=2*PARA.O3.Db/(ODE.D(76)+ODE.D(77));
ODE.kb(2,72)=2*PARA.OL.Db/(ODE.D(76)+ODE.D(77));
ODE.kb(1,73)=2*PARA.O3.Db/(ODE.D(77)+ODE.D(78));
ODE.kb(2,73)=2*PARA.OL.Db/(ODE.D(77)+ODE.D(78));
ODE.kb(1,74)=2*PARA.O3.Db/(ODE.D(78)+ODE.D(79));
ODE.kb(2,74)=2*PARA.OL.Db/(ODE.D(78)+ODE.D(79));
ODE.kb(1,75)=2*PARA.O3.Db/(ODE.D(79)+ODE.D(80));
ODE.kb(2,75)=2*PARA.OL.Db/(ODE.D(79)+ODE.D(80));
ODE.kb(1,76)=2*PARA.O3.Db/(ODE.D(80)+ODE.D(81));
ODE.kb(2,76)=2*PARA.OL.Db/(ODE.D(80)+ODE.D(81));
ODE.kb(1,77)=2*PARA.O3.Db/(ODE.D(81)+ODE.D(82));
ODE.kb(2,77)=2*PARA.OL.Db/(ODE.D(81)+ODE.D(82));
ODE.kb(1,78)=2*PARA.O3.Db/(ODE.D(82)+ODE.D(83));
ODE.kb(2,78)=2*PARA.OL.Db/(ODE.D(82)+ODE.D(83));
ODE.kb(1,79)=2*PARA.O3.Db/(ODE.D(83)+ODE.D(84));
ODE.kb(2,79)=2*PARA.OL.Db/(ODE.D(83)+ODE.D(84));
ODE.kb(1,80)=2*PARA.O3.Db/(ODE.D(84)+ODE.D(85));
ODE.kb(2,80)=2*PARA.OL.Db/(ODE.D(84)+ODE.D(85));
ODE.kb(1,81)=2*PARA.O3.Db/(ODE.D(85)+ODE.D(86));
ODE.kb(2,81)=2*PARA.OL.Db/(ODE.D(85)+ODE.D(86));
ODE.kb(1,82)=2*PARA.O3.Db/(ODE.D(86)+ODE.D(87));
ODE.kb(2,82)=2*PARA.OL.Db/(ODE.D(86)+ODE.D(87));
ODE.kb(1,83)=2*PARA.O3.Db/(ODE.D(87)+ODE.D(88));
ODE.kb(2,83)=2*PARA.OL.Db/(ODE.D(87)+ODE.D(88));
ODE.kb(1,84)=2*PARA.O3.Db/(ODE.D(88)+ODE.D(89));
ODE.kb(2,84)=2*PARA.OL.Db/(ODE.D(88)+ODE.D(89));
ODE.kb(1,85)=2*PARA.O3.Db/(ODE.D(89)+ODE.D(90));
ODE.kb(2,85)=2*PARA.OL.Db/(ODE.D(89)+ODE.D(90));
ODE.kb(1,86)=2*PARA.O3.Db/(ODE.D(90)+ODE.D(91));
ODE.kb(2,86)=2*PARA.OL.Db/(ODE.D(90)+ODE.D(91));
ODE.kb(1,87)=2*PARA.O3.Db/(ODE.D(91)+ODE.D(92));
ODE.kb(2,87)=2*PARA.OL.Db/(ODE.D(91)+ODE.D(92));
ODE.kb(1,88)=2*PARA.O3.Db/(ODE.D(92)+ODE.D(93));
ODE.kb(2,88)=2*PARA.OL.Db/(ODE.D(92)+ODE.D(93));
ODE.kb(1,89)=2*PARA.O3.Db/(ODE.D(93)+ODE.D(94));
ODE.kb(2,89)=2*PARA.OL.Db/(ODE.D(93)+ODE.D(94));
ODE.kb(1,90)=2*PARA.O3.Db/(ODE.D(94)+ODE.D(95));
ODE.kb(2,90)=2*PARA.OL.Db/(ODE.D(94)+ODE.D(95));
ODE.kb(1,91)=2*PARA.O3.Db/(ODE.D(95)+ODE.D(96));
ODE.kb(2,91)=2*PARA.OL.Db/(ODE.D(95)+ODE.D(96));
ODE.kb(1,92)=2*PARA.O3.Db/(ODE.D(96)+ODE.D(97));
ODE.kb(2,92)=2*PARA.OL.Db/(ODE.D(96)+ODE.D(97));
ODE.kb(1,93)=2*PARA.O3.Db/(ODE.D(97)+ODE.D(98));
ODE.kb(2,93)=2*PARA.OL.Db/(ODE.D(97)+ODE.D(98));
ODE.kb(1,94)=2*PARA.O3.Db/(ODE.D(98)+ODE.D(99));
ODE.kb(2,94)=2*PARA.OL.Db/(ODE.D(98)+ODE.D(99));
ODE.kb(1,95)=2*PARA.O3.Db/(ODE.D(99)+ODE.D(100));
ODE.kb(2,95)=2*PARA.OL.Db/(ODE.D(99)+ODE.D(100));
ODE.kb(1,96)=2*PARA.O3.Db/(ODE.D(100)+ODE.D(101));
ODE.kb(2,96)=2*PARA.OL.Db/(ODE.D(100)+ODE.D(101));
ODE.kb(1,97)=2*PARA.O3.Db/(ODE.D(101)+ODE.D(102));
ODE.kb(2,97)=2*PARA.OL.Db/(ODE.D(101)+ODE.D(102));
ODE.kb(1,98)=2*PARA.O3.Db/(ODE.D(102)+ODE.D(103));
ODE.kb(2,98)=2*PARA.OL.Db/(ODE.D(102)+ODE.D(103));
ODE.kb(1,99)=2*PARA.O3.Db/(ODE.D(103)+ODE.D(104));
ODE.kb(2,99)=2*PARA.OL.Db/(ODE.D(103)+ODE.D(104));

%%% SOLVER OPTIONS %%%

%Integration Tolerances
if isfield(opt,'AbsTol')
	options=odeset('AbsTol',opt.AbsTol);
	if isfield(opt,'RelTol')
		options=odeset(options,'AbsTol',opt.AbsTol,'RelTol',opt.RelTol);
	end
elseif isfield(opt,'RelTol')
	options=odeset('RelTol',opt.RelTol);
end

%Include Jacobian
options = odeset(options,'Jacobian',@(t,y)OLEICSIMPLE_ETH_sample_sce1_run_Jac(t,y,p,ODE,PARA));

%Set integration time limit
ODE.timelimit=toc+60;

%Include Events Function
options = odeset(options,'Events',@(t,y)DepletionEventsFcn(t,y,ODE,PARA));

%Output time step
t=logspace(opt.start,opt.stop,opt.n);

%%% SOLVER CALL %%%
[tint,Z] = ode23tb(@(t,y)OLEICSIMPLE_ETH_sample_sce1_run_F(t,y,p,ODE,PARA),t,y0,options);

%%% POST PROCESSING %%%

for i=1:length(species)
	eval(['RES.' species{i} '=Z(:,i);'])
end
%%% Result Matrices %%%

%%% X Output %%%

X=zeros(length(eval(data.input{1})),length(data.output));
for i=1:length(data.input)
	X(:,i)=eval(data.input{i});
end

Y=zeros(opt.n,1);
TTEMP=X;

%initial number of Y
NY0=ODE.A(4).*y0_raw(4);
for i=1:(opt.fine_bulkL+opt.bulkL)
	NY0=NY0+ODE.V(4+i).*y0_raw(4+2*i);
end

%OUTPUT
for i=1:length(data.output)
	if strcmp(data.output{i},'ny') == 1
		Y(1:size(Z,1))=ODE.A(4).*Z(:,4);
		for j=1:(opt.fine_bulkL+opt.bulkL)
			Y(1:size(Z,1))=Y(1:size(Z,1))+ODE.V(4+j).*Z(:,4+2*j);
		end
	elseif strcmp(data.output{i},'nyraw') == 1
		Y(1:size(Z,1))=ODE.A(4).*Z(:,4);
		for j=1:(opt.fine_bulkL+opt.bulkL)
			Y(1:size(Z,1))=Y(1:size(Z,1))+ODE.V(4+j).*Z(:,4+2*j);
		end
		Y=Y./NY0;
	elseif strcmp(data.output{i},'lifetime_10') == 1
		NY=zeros(size(Z,1),1);
		NY(1:size(Z,1))=ODE.A(4).*Z(:,4);
		for j=1:(opt.fine_bulkL+opt.bulkL)
			NY(1:size(Z,1))=NY(1:size(Z,1))+ODE.V(4+j).*Z(:,4+2*j);
		end
		NYRAW=NY./NY0;
		NYRAW2=NYRAW(NYRAW<0.99);
		X2=TTEMP(NYRAW<0.99);
		if length(X2) > 1 && length(unique(X2)) == length(X2) && length(unique(NYRAW2)) == length(NYRAW2) && all(diff(NYRAW2)<0)    
			Y(:,i)=interp1(NYRAW2,X2,0.1,'linear','extrap')-10^opt.start;
			XOUT(1,i)=0;
			if Y(:,i) > 10^opt.stop
				XOUT(1,i)=1/length(data.output);
			end
		elseif length(unique(NYRAW2)) ~= length(NYRAW2)
			[NYRAW3,ia]=unique(NYRAW2);
			X3=X2(ia);
			Y(:,i)=interp1(NYRAW3,X3,0.1,'linear','extrap')-10^opt.start;
		else         
			Y(:,i)=NaN;
			XOUT(1,i)=2/length(data.output);
		end
	elseif strcmp(data.output{i},'lifetime_20') == 1
		NY=zeros(size(Z,1),1);
		NY(1:size(Z,1))=ODE.A(4).*Z(:,4);
		for j=1:(opt.fine_bulkL+opt.bulkL)
			NY(1:size(Z,1))=NY(1:size(Z,1))+ODE.V(4+j).*Z(:,4+2*j);
		end
		NYRAW=NY./NY0;
		NYRAW2=NYRAW(NYRAW<0.99);
		X2=TTEMP(NYRAW<0.99);
		if length(X2) > 1 && length(unique(X2)) == length(X2) && length(unique(NYRAW2)) == length(NYRAW2) && all(diff(NYRAW2)<0)    
			Y(:,i)=interp1(NYRAW2,X2,0.2,'linear','extrap')-10^opt.start;
			XOUT(1,i)=0;
			if Y(:,i) > 10^opt.stop
				XOUT(1,i)=1/length(data.output);
			end
		elseif length(unique(NYRAW2)) ~= length(NYRAW2)
			[NYRAW3,ia]=unique(NYRAW2);
			X3=X2(ia);
			Y(:,i)=interp1(NYRAW3,X3,0.2,'linear','extrap')-10^opt.start;
		else         
			Y(:,i)=NaN;
			XOUT(1,i)=2/length(data.output);
		end
	elseif strcmp(data.output{i},'lifetime_30') == 1
		NY=zeros(size(Z,1),1);
		NY(1:size(Z,1))=ODE.A(4).*Z(:,4);
		for j=1:(opt.fine_bulkL+opt.bulkL)
			NY(1:size(Z,1))=NY(1:size(Z,1))+ODE.V(4+j).*Z(:,4+2*j);
		end
		NYRAW=NY./NY0;
		NYRAW2=NYRAW(NYRAW<0.99);
		X2=TTEMP(NYRAW<0.99);
		if length(X2) > 1 && length(unique(X2)) == length(X2) && length(unique(NYRAW2)) == length(NYRAW2) && all(diff(NYRAW2)<0)    
			Y(:,i)=interp1(NYRAW2,X2,0.3,'linear','extrap')-10^opt.start;
			XOUT(1,i)=0;
			if Y(:,i) > 10^opt.stop
				XOUT(1,i)=1/length(data.output);
			end
		elseif length(unique(NYRAW2)) ~= length(NYRAW2)
			[NYRAW3,ia]=unique(NYRAW2);
			X3=X2(ia);
			Y(:,i)=interp1(NYRAW3,X3,0.3,'linear','extrap')-10^opt.start;
		else         
			Y(:,i)=NaN;
			XOUT(1,i)=2/length(data.output);
		end
	elseif strcmp(data.output{i},'lifetime_40') == 1
		NY=zeros(size(Z,1),1);
		NY(1:size(Z,1))=ODE.A(4).*Z(:,4);
		for j=1:(opt.fine_bulkL+opt.bulkL)
			NY(1:size(Z,1))=NY(1:size(Z,1))+ODE.V(4+j).*Z(:,4+2*j);
		end
		NYRAW=NY./NY0;
		NYRAW2=NYRAW(NYRAW<0.99);
		X2=TTEMP(NYRAW<0.99);
		if length(X2) > 1 && length(unique(X2)) == length(X2) && length(unique(NYRAW2)) == length(NYRAW2) && all(diff(NYRAW2)<0)    
			Y(:,i)=interp1(NYRAW2,X2,0.4,'linear','extrap')-10^opt.start;
			XOUT(1,i)=0;
			if Y(:,i) > 10^opt.stop
				XOUT(1,i)=1/length(data.output);
			end
		elseif length(unique(NYRAW2)) ~= length(NYRAW2)
			[NYRAW3,ia]=unique(NYRAW2);
			X3=X2(ia);
			Y(:,i)=interp1(NYRAW3,X3,0.4,'linear','extrap')-10^opt.start;
		else         
			Y(:,i)=NaN;
			XOUT(1,i)=2/length(data.output);
		end
	elseif strcmp(data.output{i},'lifetime_50') == 1
		NY=zeros(size(Z,1),1);
		NY(1:size(Z,1))=ODE.A(4).*Z(:,4);
		for j=1:(opt.fine_bulkL+opt.bulkL)
			NY(1:size(Z,1))=NY(1:size(Z,1))+ODE.V(4+j).*Z(:,4+2*j);
		end
		NYRAW=NY./NY0;
		NYRAW2=NYRAW(NYRAW<0.99);
		X2=TTEMP(NYRAW<0.99);
		if length(X2) > 1 && length(unique(X2)) == length(X2) && length(unique(NYRAW2)) == length(NYRAW2) && all(diff(NYRAW2)<0)    
			Y(:,i)=interp1(NYRAW2,X2,0.5,'linear','extrap')-10^opt.start;
			XOUT(1,i)=0;
			if Y(:,i) > 10^opt.stop
				XOUT(1,i)=1/length(data.output);
			end
		elseif length(unique(NYRAW2)) ~= length(NYRAW2)
			[NYRAW3,ia]=unique(NYRAW2);
			X3=X2(ia);
			Y(:,i)=interp1(NYRAW3,X3,0.5,'linear','extrap')-10^opt.start;
		else         
			Y(:,i)=NaN;
			XOUT(1,i)=2/length(data.output);
		end
	elseif strcmp(data.output{i},'lifetime_60') == 1
		NY=zeros(size(Z,1),1);
		NY(1:size(Z,1))=ODE.A(4).*Z(:,4);
		for j=1:(opt.fine_bulkL+opt.bulkL)
			NY(1:size(Z,1))=NY(1:size(Z,1))+ODE.V(4+j).*Z(:,4+2*j);
		end
		NYRAW=NY./NY0;
		NYRAW2=NYRAW(NYRAW<0.99);
		X2=TTEMP(NYRAW<0.99);
		if length(X2) > 1 && length(unique(X2)) == length(X2) && length(unique(NYRAW2)) == length(NYRAW2) && all(diff(NYRAW2)<0)    
			Y(:,i)=interp1(NYRAW2,X2,0.6,'linear','extrap')-10^opt.start;
			XOUT(1,i)=0;
			if Y(:,i) > 10^opt.stop
				XOUT(1,i)=1/length(data.output);
			end
		elseif length(unique(NYRAW2)) ~= length(NYRAW2)
			[NYRAW3,ia]=unique(NYRAW2);
			X3=X2(ia);
			Y(:,i)=interp1(NYRAW3,X3,0.6,'linear','extrap')-10^opt.start;
		else         
			Y(:,i)=NaN;
			XOUT(1,i)=2/length(data.output);
		end
	elseif strcmp(data.output{i},'lifetime_70') == 1
		NY=zeros(size(Z,1),1);
		NY(1:size(Z,1))=ODE.A(4).*Z(:,4);
		for j=1:(opt.fine_bulkL+opt.bulkL)
			NY(1:size(Z,1))=NY(1:size(Z,1))+ODE.V(4+j).*Z(:,4+2*j);
		end
		NYRAW=NY./NY0;
		NYRAW2=NYRAW(NYRAW<0.99);
		X2=TTEMP(NYRAW<0.99);
		if length(X2) > 1 && length(unique(X2)) == length(X2) && length(unique(NYRAW2)) == length(NYRAW2) && all(diff(NYRAW2)<0)    
			Y(:,i)=interp1(NYRAW2,X2,0.7,'linear','extrap')-10^opt.start;
			XOUT(1,i)=0;
			if Y(:,i) > 10^opt.stop
				XOUT(1,i)=1/length(data.output);
			end
		elseif length(unique(NYRAW2)) ~= length(NYRAW2)
			[NYRAW3,ia]=unique(NYRAW2);
			X3=X2(ia);
			Y(:,i)=interp1(NYRAW3,X3,0.7,'linear','extrap')-10^opt.start;
		else         
			Y(:,i)=NaN;
			XOUT(1,i)=2/length(data.output);
		end
	elseif strcmp(data.output{i},'lifetime_80') == 1
		NY=zeros(size(Z,1),1);
		NY(1:size(Z,1))=ODE.A(4).*Z(:,4);
		for j=1:(opt.fine_bulkL+opt.bulkL)
			NY(1:size(Z,1))=NY(1:size(Z,1))+ODE.V(4+j).*Z(:,4+2*j);
		end
		NYRAW=NY./NY0;
		NYRAW2=NYRAW(NYRAW<0.99);
		X2=TTEMP(NYRAW<0.99);
		if length(X2) > 1 && length(unique(X2)) == length(X2) && length(unique(NYRAW2)) == length(NYRAW2) && all(diff(NYRAW2)<0)    
			Y(:,i)=interp1(NYRAW2,X2,0.8,'linear','extrap')-10^opt.start;
			XOUT(1,i)=0;
			if Y(:,i) > 10^opt.stop
				XOUT(1,i)=1/length(data.output);
			end
		elseif length(unique(NYRAW2)) ~= length(NYRAW2)
			[NYRAW3,ia]=unique(NYRAW2);
			X3=X2(ia);
			Y(:,i)=interp1(NYRAW3,X3,0.8,'linear','extrap')-10^opt.start;
		else         
			Y(:,i)=NaN;
			XOUT(1,i)=2/length(data.output);
		end
	elseif strcmp(data.output{i},'lifetime_90') == 1
		NY=zeros(size(Z,1),1);
		NY(1:size(Z,1))=ODE.A(4).*Z(:,4);
		for j=1:(opt.fine_bulkL+opt.bulkL)
			NY(1:size(Z,1))=NY(1:size(Z,1))+ODE.V(4+j).*Z(:,4+2*j);
		end
		NYRAW=NY./NY0;
		NYRAW2=NYRAW(NYRAW<0.99);
		X2=TTEMP(NYRAW<0.99);
		if length(X2) > 1 && length(unique(X2)) == length(X2) && length(unique(NYRAW2)) == length(NYRAW2) && all(diff(NYRAW2)<0)    
			Y(:,i)=interp1(NYRAW2,X2,0.9,'linear','extrap')-10^opt.start;
			XOUT(1,i)=0;
			if Y(:,i) > 10^opt.stop
				XOUT(1,i)=1/length(data.output);
			end
		elseif length(unique(NYRAW2)) ~= length(NYRAW2)
			[NYRAW3,ia]=unique(NYRAW2);
			X3=X2(ia);
			Y(:,i)=interp1(NYRAW3,X3,0.9,'linear','extrap')-10^opt.start;
		else         
			Y(:,i)=NaN;
			XOUT(1,i)=2/length(data.output);
		end
	end
end

return

