%% KM-SUB grid sampling for Kinetic Compass
% code by Thomas Berkemeier, adapted by Matteo Krüger
% Last update: 2023/08/02

function SampleGrid_KM3_20230802(iters)

%FILE LOCK
dirname='tmpfiles';
savedir='SAVE_GRID_20220923';
file_lock('create','SAMPLE_block');


%find set number
if(~exist(dirname,'dir'))
    mkdir(dirname)
end
if(~exist(savedir,'dir'))
    mkdir(savedir)
end
homedir=pwd;
cd(dirname)
for i=1:iters+1
    if exist(['SAMPLE_BLOCK_' threedigitstr(i)]) == 0
        FID1=fopen(['SAMPLE_BLOCK_' threedigitstr(i)],'a');
        fclose(FID1);
        no=i;
        break;
    elseif i == iters+1
        return;
    end
end
cd(homedir)

%choose parameter set
load('KM3_Sampling_500best_20220923','OUT_GOOD');

%REMOVE FILE LOCK
file_lock('remove','SAMPLE_block');

%sampling bounds
rad_grid=logspace(-6,-2,100); %set up grid -> must be identical to gridsprec for KC
conc_grid=logspace(10,16,100); %set up grid -> must be identical to gridsprec for KC

%initialize
i0=1;j0=1;
lambda(1:10)=[OUT_GOOD(no,1:7),0,0,1.89E+21];
cd(savedir)
if exist(['GRID_' threedigitstr(no)]) == 0
    GRID=zeros(length(rad_grid),length(conc_grid),9);
    save(['GRID_' threedigitstr(no)],'GRID');
else
    load(['GRID_' threedigitstr(no)],'GRID');
end
cd(homedir)

%work loop
for i=i0:length(rad_grid) % iterate over varied parameters, order as in envorder
    for j=j0:length(conc_grid)
        
        % assign current values
        lambda(8) = rad_grid(i); 
        lambda(9) = conc_grid(j);
        
        %save progress
        GRID(i,j,1:9) = oleic_dummy_model(lambda);
    end
    
    %save progress
    save([savedir '/GRID_' threedigitstr(no)],'GRID');    
    
end

return

%% Function "givetime" that returns hour and minute for log entry
% Thomas Berkemeier
% Last update: 2014/08/12

function time=givetime

zz=clock;

time=[num2str(zz(4),'%02d') num2str(zz(5),'%02d')];

return

%% Function "givedate" that returns hour and minute for log entry
% Thomas Berkemeier
% Last update: 2018/10/29

function date=givedate

zz=clock;

date=[num2str(zz(1),'%02d') num2str(zz(2),'%02d') num2str(zz(3),'%02d')];

return

%% Function "threedigitnum" that converts a number < 1000 into a 3 digit string
% Thomas Berkemeier
% Last update: 2015/01/10

%%% EXAMPLE %%%
% 5 -> '005'
% 23 -> '023'
% 453 -> '453'
% 3423 -> '3423'

function str=threedigitstr(num)

if num < 10
    s = num2str(num);
    str = ['00' s];
elseif num < 100
    s = num2str(num);
    str = ['0' s];
else
    str = num2str(num);
end

return

%% FILE LOCKING FUNCTION
% Thomas Berkemeier

function file_lock(op,filename)

temp=tempname();
id=temp(end-7:end);

%check if folder exists, if not, create it
if(~exist('tmpfiles','dir'))
    mkdir('tmpfiles')
end
cd('tmpfiles')

if isequal(op, 'create')

    filenamequery=[filename '*'];

    %check if lock file exists
    listing = dir(filenamequery);
    while isempty(listing) == 0
        fprintf('file lock exists(1). waiting...\n');
        pause(10);
        listing = dir(filenamequery);
    end

    while true

        %check if lock file exists
        if isempty(dir([filename '_1_*']))
            %if it doesn't exist, make one
            FID1=fopen([filename '_1_' id],'a');
            fclose(FID1);
            if length(dir([filename '_1_*'])) ~= 1
                delete([filename '_1_' id]);
                fprintf('file lock exists(2). waiting...\n');
                pause(0.5);
                continue;
            end
        else
            fprintf('file lock exists(2). waiting...\n');
            pause(5+3*rand);
            continue;
        end

        %random waiting time (0 ... 1 sec)
        pause(rand);

        %check if lock file exists
        if (isempty(dir([filename '_2_*']))) && (length(dir([filename '_1_*'])) == 1)
            %if it doesn't exist, make one
            FID2=fopen([filename '_2_' id],'a');
            fclose(FID2);
            if length(dir([filename '_2_*'])) ~= 1
                delete([filename '_1_' id]);
                delete([filename '_2_' id]);
                fprintf('file lock exists(3). waiting...\n');
                pause(0.5);
                continue;
            end
        else
            fprintf('file lock exists(3). waiting...\n');
            delete([filename '_1_' id]);
            pause(5+3*rand);
            continue;
        end

        %random waiting time (0 ... 1 sec)
        pause(rand);

        %check if lock file exists
        if isempty(dir([filename '_3_*'])) && (length(dir([filename '_1_*'])) == 1)
            %if it doesn't exist, make one
            FID3=fopen([filename '_3_' id],'a');
            fclose(FID3);
            if length(dir([filename '_3_*'])) ~= 1
                delete([filename '_1_' id]);
                delete([filename '_2_' id]);
                delete([filename '_3_' id]);
                fprintf('file lock exists(4). waiting...\n');
                pause(0.5);
                continue;
            end
        else
            fprintf('file lock exists(4). waiting...\n');
            delete([filename '_1_' id]);
            delete([filename '_2_' id]);
            pause(5+3*rand);
            continue;
        end

        %if it doesn't exist, make one
        if length(dir([filename '_1_*'])) == 1
            FID4=fopen(filename,'a');
            fclose(FID4);
            delete([filename '_3_' id]);
            delete([filename '_2_' id]);
            delete([filename '_1_' id]);
            break;
        else
            delete([filename '_3_' id]);
            delete([filename '_2_' id]);
            delete([filename '_1_' id]);
            continue;
        end

    end

elseif isequal(op, 'remove')

    %delete
    delete(filename);

end

%go back
cd('..')

return