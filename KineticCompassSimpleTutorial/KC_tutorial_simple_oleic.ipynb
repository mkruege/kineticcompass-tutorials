{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Short Tutorial for the Kinetic Compass\n",
    "\n",
    "This tutorial offers a simple introduction to the Kinetic Compass (KC). The KC is a method that integrates kinetic models, global optimization, ensemble methods, and optionally machine learning to identify experimental conditions with the greatest potential to constrain a model and its kinetic parameters. It is based on a quantification of the model's parametric uncertainty under conditions that have not been experimentally tested previously. For more detailed information, see Krüger et al. 2023. \n",
    "\n",
    "This simple introduction to the KC encompasses its basic application at the example of the ozonolysis of oleic acid and the MATLAB model KM-SUB, as described in Krüger et al. 2023. It should allow the reader to apply the KC on his own system. This short tutorial is a summary of the individual steps and options that you can chose from. It is not intended to be run from top to bottom, but rather provide the individual code blocks that have to be assembled for a specific application. If you rather want to have a more \"linear\" tutorial that addresses these options individually, see *KC_tutorial_simple_linear_oleic*.\n",
    "\n",
    "Furthermode, the KC allows the implementation of self-written modules (constraint potential metrics, models, optimizers) that are tailored to a specific application. If you are interesed in making such modules, please consider the *KC_tutorial_advanced* after reading this tutorial."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "At first, we make an environment and install the KineticCompass and all dependencies. After you have successfully installed the KineticCompass, you can omit line 5 in the future (your environment with the Kinetic Compass only needs to be activated then)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "using Pkg\n",
    "# specify path to your environment:\n",
    "Pkg.activate(joinpath(homedir(), \".julia\", \"environments\", \"kinetic_compass_oleic_tutorial\"))\n",
    "\n",
    "Pkg.add(url=\"https://gitlab.mpcdf.mpg.de/mkruege/kineticcompass\") # install package or check for updates\n",
    "\n",
    "using KineticCompass"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Among model input parameters, we differentiate between kinetic parameters that define the physical and chemical properties of the modelled system (e.g. reaction rate coefficients), and parameters that define the environmental or experimental conditions. \n",
    "\n",
    "**1: Kinetic parameters:** A multitude of combinations of kinetic parameters that lead to model outputs in agreement with previous experimental data, a so-called *fit ensemble*, is required by the KC and represents a sample from the model's current *solution space*. You need to acquire such a fit ensemble before you can apply the KC. In principle, you need to sample kinetic parameter sets (randomly, or using optimization), evaluate an error metric in comparison with the experimental data and accept all kinetic parameter sets that fall below a specified error threshold.\n",
    "\n",
    "The fit ensemble can be loaded from a .csv file, specified in **'fitfile::AbstractString'**. Each column in this file refers to a kinetic parameter, each row to a fit. You further have to specify the number of fits that you want to use. The KC will use the top **'n_fits::Int'** rows of the fitfile. \n",
    "\n",
    "Note that this fit ensemble must not contain any experimental parameters (since they are set to the conditions of the previous experiments during fit acqusition).\n",
    "\n",
    "If the file contains further information like errors or cluster labels (as columns), you can specify these column names under **'separate_fitfile_columns::Vector{String}'**. Only add such, if they are actually expected by the constraint potential metric that you want to use (e.g. ensemble_label_distance, or weighted_parameter_constraint_potential). In any other case, delete such additional columns from your file, or they will be considered kinetic parameters by the KC. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fitfile = \"km3_fits_sorted_resampled.csv\" # path to your fit ensemble csv file only containing kinetic parameters of fits\n",
    "n_fits = 20 # number of fits that are read from this file (from top)\n",
    "\n",
    "# Only add one or multiple column headers like \"labels\" to the 'separate_fitfile_columns', if you apply a constraint potential metric that requires additional information about each fit! \n",
    "# This column needs to be present in the fitfile and will NOT be regarded as kinetic parameter by the KC\n",
    "# this file without an additional column can be used for the ensemble spread and regular (unweighted) parameter constraint potential\n",
    "separate_fitfile_columns = String[] # pass an empty vector of strings if no separate columns are present\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# ONLY RUN THIS CELL IF YOU WANT TO USE THE ENSEMBLE_LABEL_DISTANCE METRIC\n",
    "\n",
    "fitfile = \"km3_fits_sorted_resampled_labeled.csv\" # path to your fit ensemble csv file including a column \"label\"\n",
    "n_fits = 20 # number of fits that are read from this file (from top)\n",
    "\n",
    "\n",
    "separate_fitfile_columns = [\"label\"] # this column must be present in the file and is expected to be passed to the CP-metric, but is NOT a model parameter"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# ONLY RUN THIS CELL IF YOU WANT TO USE PARAMETER_CONSTRAINT_POTENTIAL WITH ERROR WEIGHTING\n",
    "\n",
    "fitfile = \"km3_fits_sorted_resampled_witherr.csv\" # path to your fit ensemble csv file including a column \"error\"\n",
    "n_fits = 20 # number of fits that are read from this file (from top)\n",
    "\n",
    "separate_fitfile_columns = [\"error\"] # this column must be present in the file and is expected to be passed to the CP-metric, but is NOT a model parameter"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "2: Experimental parameters: The Kinetic Compass operates on a grid of a variable number of experimental (or environmental) parameters that define the conditions for a prospective experiment. This grid must initially be defined by the argument **'gridspec::Dict{String, Vector{Any}}'**. Each entry in this dictionary has one varied experimental parameter name as key, associated with a vector that specifies: \n",
    "\n",
    "*[[lower_boundary::Float64, upper_boundary::Float64], grid_points::Int, logspace(0 or 1)::Int]*\n",
    "\n",
    "In addition to the boundaries and the number of points on the grid, the last element indicates if the grid is initialized in linspace (0) or logspace(1). Note that upper and lower boundaries are always provided in linspace, even if the grid is generated in logspace!\n",
    "\n",
    "Some experimental parameters can be set to unmutable values, e.g. to reduce dimensionality and increase performance of the KC. Or because they are just not varied in a prospective experiment. For this, we use the argument **'set_params::Dict{String, Float64}'**, that simply associates the parameter name (key) with the set value.\n",
    "\n",
    "As the Kinetic Compass allows the evaluation of (external) models, we further need to define the order, in which experimental parameters are passed to the model. We define this in **'envorder::Vector{String}'**. Note that the Kinetic Compass expects the experimental parameters to be passed to a model **after** the kinetic parameters in any case: model_inputs = [kinetic_parameters_from_fit_ensemble, mutable_and_set_exp_parameter_as_in_envorder]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# example system with 2 experimental parameters varied:\n",
    "gridspec = Dict(\n",
    "    \"particle_size\" => [[10^(-6.0), 10^(-2.0)], 100, 1],\n",
    "    \"[O3]\" => [[10^(10.0), 10^(16.0)], 100, 1],\n",
    "    )\n",
    "set_params::Dict{String, Float64} = Dict(\"[Ol]\" => 1.89E+21) #...and one parameter set to specific value\n",
    "envorder = [\"particle_size\", \"[O3]\", \"[Ol]\"] # if a model is called, it will be passed [kinetic_params..., \"particle_size\", \"[O3]\", \"[Ol]\"]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To evaluate constraint potentials for potential experiments on this grid, we need to evaluate a model for all fits in the fit ensemble for the specific experimental conditions that we want to test. There are two ways to do this:\n",
    "\n",
    "Option 1: \"External\" model evaluation for the full grid (identical to gridspec) and specification of a directory that contains files with all predictions. This is currently implemented for .m (MATLAB) files. Note that the order of varied experimental parameters in the grid loop must be identical to the one specified in \"envorder\". An example script for the generation of such data using a MATLAB model is provided in the file 'SampleGrid_KM3_20230802.m'.\n",
    "\n",
    "This is recommended, if your model is fast enough to scan a reasonably large grid of experimental conditions and if you want to get a general overview of the constraint potentials in your system (plotting). While the KC can also use a model (Option 2) to evaluate the full grid for identical results at runtime, you can usually expect a faster calculation in the model's \"home environment\" (MATLAB/Python/...)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "model_output_grid = \"KM3_GridSampling_20220924\" # path to directory with .m data; must match gridspec!\n",
    "\n",
    "output_model = nothing # no model required, if grid is provided"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "After first evaluation (if you evaluate the full grid), the KC will create a .jls file to save the full in- and output grid. You can pass this file as argument for the following runs - this will make re-evaluation faster, especially if you used a model to evaluate the grid at runtime of the KC! Note that this file becomes useless, if you modify **gridspec**."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# ONLY RUN THIS CELL IF YOU HAVE CREATED A .JLS IN A PREVIOUS KC RUN\n",
    "\n",
    "model_output_grid = \"current_full_grid.jls\" # path to the previously saved grid data\n",
    "\n",
    "output_model = nothing # no model required, if grid is provided"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Option 2: You can pass a function to the KC that calls the model at runtime. An example for a MATLAB model is shown in the following code block. You can write a similar Julia wrapper function and pass it to the KC as **'output_model::Function'** to apply your own model. It must match the following in- and outputs (see MATLAB example):\n",
    "\n",
    "Input: The input of this function is a Vector of DataFrames, one DataFrame for each experimental condition that is to be evaluated. Each DataFrame has **n_fits** rows and (n_kinetic_parameters + n_experimental_parameters) columns, the experimental parameters ordered as in **envorder**.\n",
    "\n",
    "Output: The function can either return a Vector of DataFrames in a similar fashion (and the same order as the input DataFrames), or a Vector (exp. conditions) of sub-vectors (fit in fitfile) of sub-vectors (model outputs) of Float64.\n",
    "\n",
    "Additional arguments: You can add further optional arguments to this wrapper function, if required. They can be defined before KC evaluation using lambda functions (see code blocks for ensemble_spread, parameter_constraint_potential)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# DO NOT RUN THIS CELL IF YOU HAVE PROVIDED A PRE-EVALUATED INPUT GRID\n",
    "Pkg.add(\"MATLAB\")\n",
    "using MATLAB, DataFrames\n",
    "\n",
    "function call_MATLAB_dummy_model(all_params::Vector{DataFrame})::Vector{Vector{Vector{Float64}}}\n",
    "    all_params_preds = []\n",
    "\n",
    "    # loop over each DataFrame in all_params\n",
    "    for (i, params_df) in enumerate(all_params)\n",
    "        if length(all_params) > 1\n",
    "            println(\"Obtaining ensemble solution $i of $(length(all_params))\")\n",
    "        end\n",
    "        params_preds = []\n",
    "\n",
    "        # loop over each lambda\n",
    "        for (j, lambda_row) in enumerate(eachrow(params_df))\n",
    "            lambda = Vector(lambda_row)\n",
    "\n",
    "            # go into directory (catch if already there)\n",
    "            try\n",
    "                mat\"\"\"cd oleic_dummy_model\"\"\"\n",
    "            catch\n",
    "            end\n",
    "\n",
    "            # construct the MATLAB expression using the mat\"\" literal\n",
    "            mat\"\"\"\n",
    "                warning('off','all')\n",
    "                $result = exec_oleic_dummy_model(transpose($lambda));\n",
    "                cd ..\n",
    "            \"\"\"\n",
    "            push!(params_preds, vec(result))\n",
    "        end\n",
    "        push!(all_params_preds, params_preds)\n",
    "    end\n",
    "    \n",
    "    return all_params_preds\n",
    "end\n",
    "\n",
    "\n",
    "output_model = (all_params) -> call_MATLAB_dummy_model(all_params) # make lambda function: not necessary but can be used to set optional arguments\n",
    "model_output_grid = nothing"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The Kinetic Compass evaluates the space of possible experiments specified in **gridspec** with regards to a metric for constraint potential (CP). The two metrics from Krüger et al. 2023 (ensemble spread and parameter constraint potential) are called in the following code cells. You can simply pass one of these functions as **constraint_potential_metric_func::Function** and set the non-positional (optional) arguments using anonymous (lambda) functions:\n",
    "\n",
    "**Only run one of the following cells! Make sure that your CP metric matches your 'sepeate_columns' (fit ensemble file):**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "CP_metric_with_args = (X) -> begin\n",
    "\n",
    "    # ENSEMBLE SPREAD (ES) - specify additional arguments:\n",
    "    local n_interp::Int = 0 # if outputs are interpolated, and to how many points; 0 -> no interpolation\n",
    "    local x_vec::Vector{Float64} = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9] # if n_interp = true, please provide the x-vector for the model outputs (y)\n",
    "    local normalize::Bool = true # if the ensemble spread is normalized by the area under the ensemble mean\n",
    "    local nan_is_zero::Bool = true # true: NaN values in model outputs (solutions) are set to 0; false: model outputs containing NaN are deleted from ensemble before calculation\n",
    "    local delete_0rows::Bool = true # true: if ALL model outputs are 0 in a solution, it is omitted for calculation\n",
    "\n",
    "    # don't modify this:\n",
    "    KineticCompass.KCModules.ensemble_spread(X; n_interp=n_interp, x_vec=x_vec, normalize=normalize, nan_is_zero=nan_is_zero, delete_0rows=delete_0rows) # make lambda function including specified arguments\n",
    "end\n",
    "\n",
    "constraint_potential_metric_name = \"ensemble spread\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# DO NOT RUN THIS CELL IF ENSEMBLE SPREAD IS SELECTED AS CP-METRIC\n",
    "\n",
    "CP_metric_with_args = (all_params_preds, all_params) -> begin\n",
    "\n",
    "    # PARAMETER CONSTRAINT POTENTIAL (PCP) - specify additional arguments:\n",
    "    local error_metric::Function = KineticCompass.KCModules.logabs_error_metric # error metric to determine hypothetical fit acceptance - mse_error_metric and logabs_error_metric provided in Modules.jl\n",
    "    local error_th::Float64 = 0.015  # error threshold to determine hypothetical fit acceptance - consider using the error metric + threshold from fit acquisition of your original fit ensemble\n",
    "    local sample_density::Int = 1 # sample density, see Krüger et al. 2023 SI (when applied (>1) it is recommended to use: (n_fits modulo sample_density = 1))\n",
    "    local sortbyout::Union{Int, AbstractString} = \"midval\" # column that is used for initial separation into \"negative and positive errors\"; default \"midval\": half-time; see Krüger et al. SI\n",
    "    local logparams::Bool = true # logarithmize parameters\n",
    "    local logpreds::Bool = false # logarithmize predictions (consider error metric!)\n",
    "    local nan_is_zero::Bool = true # true: NaN values in model outputs (solutions) are set to 0; false: model outputs containing NaN are deleted from ensemble before calculation\n",
    "    local delete_0rows::Bool = true # true: if ALL model outputs are 0 in a solution, it is omitted for calculation\n",
    "    local comparability::String = \"standardize\" # options to make constraint potentials comparable between multiple parameters: \"none\" nothing, \"normalize\" normalization of CP by difference between boundaries, \"standardize\" standardization of all param values\n",
    "\n",
    "    # don't modify this:\n",
    "    KineticCompass.KCModules.parameter_constraint_potential(all_params_preds, all_params; error_metric=error_metric, error_th=error_th, sample_density=sample_density, nan_is_zero=nan_is_zero, delete_0rows=delete_0rows, sortbyout=sortbyout, logparams=logparams, logpreds=logpreds, comparability=comparability) # make lambda function including specified arguments\n",
    "end\n",
    "\n",
    "constraint_potential_metric_name = \"parameter constraint potential\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# ONLY RUN THIS CELL IF YOU HAVE PASSED A SEPARATE_COLUMN THAT CONTAINS ERRORS (FLOATS) FOR THE WEIGHTING OF EACH FIT; AND IF ENSEMBLE SPREAD IS NOT SELECTED AS CP-METRIC\n",
    "\n",
    "CP_metric_with_args = (all_params_preds, all_params, sepatate_columns) -> begin\n",
    "\n",
    "    # PARAMETER CONSTRAINT POTENTIAL WITH FIT WEIGHTING (PCP) - specify additional arguments:\n",
    "    local error_metric::Function = KineticCompass.KCModules.logabs_error_metric # error metric to determine hypothetical fit acceptance - mse_error_metric and logabs_error_metric provided in Modules.jl\n",
    "    local error_th::Float64 = 0.015  # error threshold to determine hypothetical fit acceptance - consider using the error metric + threshold from fit acquisition of your original fit ensemble\n",
    "    local sample_density::Int = 1 # sample density, see Krüger et al. 2023 SI (when applied (>1) it is recommended to use: (n_fits modulo sample_density = 1))\n",
    "    local sortbyout::Union{Int, AbstractString} = \"midval\" # column that is used for initial separation into \"negative and positive errors\"; default \"midval\": half-time; see Krüger et al. SI\n",
    "    local logparams::Bool = true # parameters are logarithmized by the function (must be false if they already are)\n",
    "    local logpreds::Bool = false # predictions are logarithmized by the function (consider error metric! Must be false if they already are)\n",
    "    local nan_is_zero::Bool = true # true: NaN values in model outputs (solutions) are set to 0; false: model outputs containing NaN are deleted from ensemble before calculation\n",
    "    local delete_0rows::Bool = true # true: if ALL model outputs are 0 in a solution, it is omitted for calculation\n",
    "    local comparability::String = \"standardize\" # options to make constraint potentials comparable between multiple parameters: \"none\" nothing, \"normalize\" normalization of CP by difference between boundaries, \"standardize\" standardization of all param values\n",
    "    # don't modify this:\n",
    "    KineticCompass.KCModules.parameter_constraint_potential(all_params_preds, all_params, sepatate_columns; error_metric=error_metric, error_th=error_th, sample_density=sample_density, nan_is_zero=nan_is_zero, delete_0rows=delete_0rows, sortbyout=sortbyout, logparams=logparams, logpreds=logpreds, comparability=comparability) # make lambda function including specified arguments\n",
    "end\n",
    "\n",
    "    constraint_potential_metric_name = \"weighted parameter constraint potential\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# ONLY RUN THIS CELL IF YOU HAVE PASSED A SEPARATE_COLUMN THAT CONTAINS LABELS (INT); AND IF NEITHER ENSEMBLE SPREAD NOR PARAMETER CONSTRAINT POTENTIAL ARE SELECTED AS CP-METRIC\n",
    "\n",
    "CP_metric_with_args = (all_params_preds, separate_columns) -> begin\n",
    "\n",
    "    # ENSEMBLE LABEL DISTANCE (ELD) - specify additional arguments:\n",
    "    local logdis::Bool = true # if logarithmic distance is calculated\n",
    "    local normalize::Bool = false # if distances are normalized by ensemble standard deviation\n",
    "\n",
    "    KineticCompass.KCModules.ensemble_label_distance(all_params_preds, separate_columns; logdis, normalize) # make lambda function including specified arguments\n",
    "end\n",
    "\n",
    "constraint_potential_metric_name = \"ensemble label distance\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# DO NOT RUN THIS CELL IF ENSEMBLE SPREAD, PARAMETER CONSTRAINT POTENTIAL OR ENSEMBLE LABEL DISTANCE ARE SELECTED AS CP-METRIC\n",
    "\n",
    "CP_metric_with_args = (all_params_preds, separate_columns) -> begin\n",
    "\n",
    "    # TARGET CONSTRAINT POTENTIAL (TCP) - specify additional arguments:\n",
    "    local error_metric::Function = KineticCompass.KCModules.logabs_error_metric, # error metric to determine hypothetical fit acceptance\n",
    "    local error_th::Float64 = 0.015, # error threshold to determine hypothetical fit acceptance\n",
    "    local n_interp::Int = 0, # if target ensemble solutions are interpolated, and to how many points\n",
    "    local normalize::Bool = true, # if the ensemble spread is normalized by the area under the ensemble mean\n",
    "    local sample_density::Int = 1, # sample density, see Krüger et al. SI (recommended: (n_fits modulo sample_density = 1))\n",
    "    local nan_is_zero::Bool = true, # if NaNs in the model outputs are considered as 0 (if false, nans will be deleted before evaluation)\n",
    "    local delete_0rows::Bool = true, # if all model outputs are 0, this solution is omitted for calculation\n",
    "    local sortbyout::Union{Int, AbstractString} = \"midval\", # column that is used for initial separation into \"negative and positive errors\"; default: half-time; see Krüger et al. SI\n",
    "    local logpreds::Bool = false # if predictions are logarithmized by the function (consider error metric! Must be false if they already are)\n",
    "    # don't modify this:\n",
    "    KineticCompass.KCModules.target_constraint(all_params_preds, separate_columns; error_metric=error_metric, error_th=error_th, n_interp=n_interp, normalize=normalize, sample_density=sample_density, nan_is_zero=nan_is_zero, delete_0rows=delete_0rows, sortbyout=sortbyout, logpreds=logpreds) # make lambda function including specified arguments\n",
    "end\n",
    "# set your target conditions (for which set of env. conditions do you want to improve your model?)\n",
    "KineticCompass.set_target(Dict(\"particle_size\" => 10^(-4.5), \"[O3]\" => 10^(11.0), \"[Ol]\" => 1.89E+21))\n",
    "constraint_potential_metric_name = \"target constraint potential\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Furthermore, there are two options to evaluate the KC method:\n",
    "\n",
    "Option 1: Evaluation of full grid, required for plotting -> Recommended if full model prediction grid is provided; also possible without pre-evaluated grid by evaluation of the model. If you are using MATLAB/Python/... models, consider evaluating the full grids within MATLAB/Python/... for optimal performance."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# set this to true, if you want an constraint potential evaluation of the full input grid\n",
    "evaluate_full_input_grid = true\n",
    "optimizer_with_args = nothing"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Option 2: Application of an optimization algorithm to find a CP maximum in the grid. This way, only a small fraction of the grid will be evaluated by the model. This is recommended, if the model is slow and/or the space of prospective experiments (grid) very large, making full evaluation computationally too expensive.\n",
    "\n",
    "The optimization algorithm will search for a CP maximum on the discrete grid. You can set the number of grid points (in **'gridspec'**) accordingly, to ensure a sufficient resolution (e.g. considering uncertainty of experimental parameters; remember to adapt discrete learning_rate (maximum discrete step size) and decay [n_evaluations before reduction of learning rate by 1] accordingly). The following code block calls a simple discrete optimization algorithm from KCModules.jl for a model passed as the argument **'output_model::Function'**:\n",
    "\n",
    "(It is possible to run an optimization on a pre-evaluated grid, but this is generally not recommended. The KC framework is very fast, and model evaluation of the grid usually the computational bottleneck of the method. Thus, if you have a pre-evaluated grid for model outputs, consider applying KC on the full grid.)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# DO NOT RUN THIS CELL IF FULL GRID EVALUATION IS DESIRED!\n",
    "\n",
    "optimizer_with_args = (model, constraint_potential_metric_func, output_directory, all_params_dict, separate_columns, envorder) -> begin\n",
    "        # simple, discrete optimizer\n",
    "        local num_iterations::Int = 20 # number of iterations per optimization\n",
    "        local learning_rate::Int = 20 # maximum step size in each dimension\n",
    "        local decay::Int = 1 # number of iterations before learning_rate is decreased by 1\n",
    "        local tolerance::Float64 = 0.0 # tolerance to quit if no sufficient progress\n",
    "        local i_CP::Int = 1 # index of constraint potential metric function output that is optimized (always 1 for ensemble spread, kinetic_param_index for parameter CP)\n",
    "        \n",
    "        KineticCompass.KCModules.discrete_optimizer_sgd(model, constraint_potential_metric_func, output_directory, all_params_dict, separate_columns, envorder;\n",
    "                num_iterations=num_iterations, learning_rate=learning_rate, decay=decay, tolerance=tolerance, i_CP=i_CP)\n",
    "end\n",
    "\n",
    "evaluate_full_input_grid = false # when using an optimizer, you have to set this to false; if true, full grid evaluation overrules the optimizer"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, we need to specify the remaining arguments...: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "output_directory = \"test_out\" # relative path to result directory (from current pwd()), will be created if not present\n",
    "\n",
    "verbose = true # if false: status updates are omitted"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "... and call the Kinetic Compass. Ahoy!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "KineticCompass.run_kinetic_compass(output_directory, fitfile, n_fits, constraint_potential_metric_name, \n",
    "    CP_metric_with_args, gridspec, set_params, \n",
    "    envorder; CP_optimizer_func=optimizer_with_args, model_output_grid=model_output_grid, output_model=output_model, separate_fitfile_columns=separate_fitfile_columns, \n",
    "    evaluate_full_input_grid=evaluate_full_input_grid, verbose=verbose)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The results and plots are saved in the specified directory, including a file *KC_evaluation_full_grid.jls*. At first KC execution, plots are saved in default mode. For more advanced plotting, you can later call the function plot_contour() and pass the path to the *KC_evaluation_full_grid.jls* as first argument. This will allow you to quickly and easily plot variants of previously calculated results. Of course, you can also use the csv result file to implement your own plotting in your preferred language and environment. \n",
    "\n",
    "Note that if you have more than two varied experimental parameters, there is a potentially very large number of two-dimensional contourplots that you can generate. The implemented function plots all two-dimensional cuts of the CP hypersurface that contain the absolute CP maximum."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# add scatter points for the experiments that have been used to obtain the fit ensemble:\n",
    "prev_experiments = [[2.70E-03, 3.69E+14], [2.70E-03, 4.92E+12], [2.00E-05, 7E+13], [4.00E-05, 2.50E+15], [2.50E-05, 2E+14], [2.50E-05, 3.25E+14], [2.50E-05, 5.51E+14]]\n",
    "# define settings for the colormap:\n",
    "colmap = :turbo # color scheme\n",
    "levels = 40 # levels in contourplot\n",
    "cp_lims = (0.0, 2.0) # color range of the map\n",
    "barcolor = :blue # color for barplot\n",
    "KineticCompass.plot_contour(joinpath(\"test_out\", \"KC_evaluation_full_grid.jls\"), joinpath(pwd(), \"plot_test\"); cp_lims=cp_lims, colmap=colmap, levels=levels, prev_experiments=prev_experiments)\n",
    "KineticCompass.plot_barplot(joinpath(\"test_out\", \"KC_evaluation_full_grid.jls\"), joinpath(pwd(), \"plot_test\"); barcolor=barcolor, prev_experiments=prev_experiments)\n",
    "KineticCompass.plot_multi_cp_comparison(joinpath(\"test_out\", \"KC_evaluation_full_grid.jls\"), joinpath(pwd(), \"plot_test\"))"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Julia 1.9.2",
   "language": "julia",
   "name": "julia-1.9"
  },
  "language_info": {
   "file_extension": ".jl",
   "mimetype": "application/julia",
   "name": "julia",
   "version": "1.9.2"
  },
  "orig_nbformat": 4
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
