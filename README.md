# KineticCompass Tutorials

This repository contains tutorials and example applications of the Kinetic Compass (KC) framework for experiment design/prioritization to optimize model (parameter) constraints. The KC can be applied to any process model or system and allows the implementation of additional modules that are specifically tailored to such.

The tutorials provide an example MATLAB model for the heterogeneous ozonolysis of oleic-acid along with a fit ensemble, a pre-calculated grid of model outputs and example scripts for the generation of such. The Jupyter Notebooks are the actual tutorials, addressing the individual inputs and modules for the KineticCompass. You can adapt them to your specific system step-by-step.

In addition to the 'KC_tutorial_simple_oleic' tutorial that concisely explains all different options to chose from, 'KC_tutorial_simple_linear_oleic' encompasses six example applications that introduce the different options, features and their combinations one by one, intended to be run top-to-bottom. It is recommended to read the linear tutorial first, before using the regular tutorial as a 'cheat sheet' to adapt the KineticCompass to your own application.

In addition, you will find an advanced tutorial which addresses the implementation of modules like constraint potential metrics or optimization algroithms into the KC framework. This will allow you to specifically adapt the method for research questions associated with your own model and underlying system. If you wish to share your modules and add them to the KCModules library, or if you need help implementing a function, please don't hesitate to contact m.krueger@mpic.de.


## Julia Package

The KineticCompass package can be downloaded here:

https://gitlab.mpcdf.mpg.de/mkruege/kineticcompass

Alternatively, can be installed in Julia including all dependencies with the following command:
```
Pkg.add(url="https://gitlab.mpcdf.mpg.de/mkruege/kineticcompass")
```


## Documentation

The tutorials provide a step-by-step explanation of all options, modules and function arguments. They are designed to allow an easy access to the functionalities of this package and are recommended as introduction for anyone who wants to apply the KineticCompass. For a more concise documentation, as well as a changelog, please look up the README at https://gitlab.mpcdf.mpg.de/mkruege/kineticcompass.


## Authors and acknowledgment

Julia code and tutorials: Matteo Krüger  
MATLAB oleic-acid model and grid evaluation script: Thomas Berkemeier
